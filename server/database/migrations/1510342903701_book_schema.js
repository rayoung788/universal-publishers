'use strict'

const Schema = use('Schema')

class BookSchema extends Schema {
  up () {
    this.create('books', (table) => {
      table.increments()
      table.string('title')
      table.string('subtitle')
      table.string('author')
      table.string('category')
      table.string('publisher')
      table.integer('pages')
      table.integer('popularity')
      table.integer('year')
      table.text('synopsis')
      table.string('isbn10')
      table.string('isbn13')
      table.timestamps()
    })
  }

  down () {
    this.drop('books')
  }
}

module.exports = BookSchema
