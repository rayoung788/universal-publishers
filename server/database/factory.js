'use strict'

/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

const Factory = use('Factory')

/**
  Factory.blueprint('App/Models/User', (faker) => {
    return {
      username: faker.username()
    }
  })
*/

Factory.blueprint('App/Models/Book', (fake) => {
  return {
    title: fake.sentence({words: fake.integer({min: 2, max: 5})}),
    subtitle: fake.sentence(),
    author: fake.name(),
    category: fake.pickone(['Art & Architecture', 'Business & Economics', 'Computers & Electronics', 'Education', 'Geography & GIS', 'History & Biography', 'Language & Lingusitics', 'Literature & Literary Criticism', 'Law & Legal Studies', 'Mathematics', 'Medicine & Health', 'Music & Fine Arts', 'Biology & Natural Sciences', 'Philosophy, & Theology', 'Political Science', 'Psychology', 'Reference Guides', 'Physics & Chemistry', 'Technology & Engineering', 'Social Science']),
    publisher: fake.pickone(['BrownWalker Press', 'Universal-Publishers', 'Dissertation.com']),
    pages: fake.integer({min: 50, max: 1000}),
    popularity: fake.integer({min: 0, max: 10000}),
    year: fake.year({min: 1990, max: 2017}),
    synopsis: fake.paragraph(),
    isbn10: fake.string({length: 10, pool: '1234567890'}),
    isbn13: fake.string({length: 13, pool: '1234567890'})
  }
})
