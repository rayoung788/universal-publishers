'use strict'

const Book = use('App/Models/Book')

class BookController {
    async category({ params, request, response }) {
        const category = params.category.replace(/%20/g, " ")
        const page = parseInt(request.input('page', 1))
        const posts = (category !== 'Featured') ? await  Book.query().where('category', category).orderBy('year', 'desc').paginate(page, 3) : await  Book.query().orderBy('popularity', 'desc').paginate(page, 3)
        response.json(posts)
    }

    async details({ params, response }) {
        const id = parseInt(params.id || 1)
        let book = await  Book.find(id)
        if (!book) {
          book = {'error': 'Sorry, cannot find the selected id'}
        }
        response.json(book)
    }

    async newest({ request, response }) {
        const newest = await  Book.query().orderBy('year', 'desc').limit(3)
        response.json({newest: newest})
    }

    async search({ request, response }) {
        const search = request.input('term').replace(/%20/g, " ")
        const page = parseInt(request.input('page', 1))
        const posts = await  Book.query().where(
            'title', 'like', `%${search}%`
        ).orWhere(
            'subtitle', 'like', `%${search}%`
        ).orWhere(
            'author', 'like', `%${search}%`
        ).orWhere(
            'synopsis', 'like', `%${search}%`
        ).orderBy('year', 'desc').paginate(page, 6)
        response.json(posts)
    }
}

module.exports = BookController
