'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.any('/', ({ view }) => {
    return view.render('index')
  })

Route.get('books/category/:category', 'BooksController.category')

Route.get('books/details/:id', 'BooksController.details')

Route.get('books/newest', 'BooksController.newest')

Route.get('books/search', 'BooksController.search')
