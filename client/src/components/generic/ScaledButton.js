import { VBtn } from 'vuetify'

export default {
  name: 'scaled-btn',
  mixins: [VBtn],
  computed: {
    classes: function() {
      return {
        ...VBtn.computed.classes.call(this),
        mobile: this.$vuetify.breakpoint.xsOnly,
        medium: this.$vuetify.breakpoint.mdAndDown && !this.$vuetify.breakpoint.xsOnly,
        'custom-btn': true
      }
    }
  }
}
