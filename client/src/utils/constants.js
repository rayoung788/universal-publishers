// const categories = [
//     { left: 'Art & Architecture', center: 'Literature & Criticism', right: 'Political Science' },
//     { left: 'Business & Economics', center: 'Law & Legal Studies', right: 'Psychology' },
//     { left: 'Computers & Electronics', center: 'Mathematics', right: 'Physics & Chemistry' },
//     { left: 'Education', center: 'Medicine & Health', right: 'Reference Guides' },
//     { left: 'Geography & GIS', center: 'Music & Fine Arts', right: 'Tech & Engineering' },
//     { left: 'History & Biography', center: 'Biology & Nature', right: 'Social Science' },
//     { left: 'Language & Lingusitics', center: 'Philosophy & Theology', right: 'Textbooks & Study Aids' }

//     // { left: 'Art & Architecture', right: 'Music & Fine Arts' },
//     // { left: 'Business & Economics', right: 'Biology & Nature' },
//     // { left: 'Computers & Electronics', right: 'Philosophy, & Theology' },
//     // { left: 'Education', right: 'Political Science' },
//     // { left: 'Geography & GIS', right: 'Psychology' },
//     // { left: 'History & Biography', right: 'Reference Guides' },
//     // { left: 'Language & Lingusitics', right: 'Physics & Chemistry' },
//     // { left: 'Literature & Criticism', right: 'Tech & Engineering' },
//     // { left: 'Law & Legal Studies', right: 'Social Science' },
//     // { left: 'Mathematics', right: 'Textbooks & Study Guides' },
//     // { left: 'Medicine & Health', right: 'Conferences & Journals' }
// ]

const categories = [
    'Art & Architecture',
    'Business & Economics',
    'Computers & Electronics',
    'Education',
    'Geography & GIS',
    'History & Biography',
    'Language & Lingusitics',
    'Literature & Criticism',
    'Law & Legal Studies',
    'Mathematics',
    'Medicine & Health',
    'Music & Fine Arts',
    'Biology & Nature',
    'Philosophy & Theology',
    'Political Science',
    'Psychology',
    'Reference Guides',
    'Physics & Chemistry',
    'Tech & Engineering',
    'Social Science',
    'Textbooks & Study Aids',
    'Conferences & Journals'
]

const links = {
    bookstore: '/bookstore/Featured/1',
    publishing: '/publishing',
    proposals: '/proposals',
    welcome: '/welcome',
    siteMap: '/site_map'
}

export default {
    categories,
    links
}

