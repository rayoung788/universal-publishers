import axios from 'axios'
const host = '/books'
// const im = 'http://localhost:5555/' http://172.0.113.77:5555

axios.defaults.withCredentials = true

const newest = () => {
    return axios.get(`${host}/newest`).then((response) => {
        return response.data
    })
}

const details = (id) => {
    return axios.get(`${host}/details/${id}`).then((response) => {
        return response.data
    })
}

const category = (category, page = 1) => {
    return axios.get(`${host}/category/${category}?page=${page}`).then((response) => {
        return response.data
    })
}

const search = (term, page = 1) => {
    console.log(`${host}/search?term=${term}&page=${page}`)
    return axios.get(`${host}/search?term=${term}&page=${page}`).then((response) => {
        console.log(response)
        return response.data
    })
}

export default {
    details,
    newest,
    category,
    search,
    host
}
