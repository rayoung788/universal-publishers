import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/pages/main/Welcome'
import Bookstore from '@/components/pages/main/Bookstore'
import Book from '@/components/books/Book'
import Publishing from '@/components/pages/main/Publishing'
import Proposals from '@/components/pages/main/Proposals'
import SiteMap from '@/components/pages/main/SiteMap'
import Royalties from '@/components/pages/off/Royalties'
import Upload from '@/components/pages/off/Upload'
import MiscPayments from '@/components/pages/off/MiscPayments'
import Agreement from '@/components/pages/off/Agreement'
import Submission from '@/components/pages/off/Submission'
import Search from '@/components/books/Search'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/welcome',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/bookstore/:category/:page',
      name: 'Bookstore',
      component: Bookstore
    },
    {
      path: '/book/:id',
      name: 'Book',
      component: Book
    },
    {
      path: '/publishing',
      name: 'Publishing',
      component: Publishing
    },
    {
      path: '/proposals',
      name: 'Book Proposals',
      component: Proposals
    },
    {
      path: '/royalties',
      name: 'Author Royalties',
      component: Royalties
    },
    {
      path: '/upload',
      name: 'Upload',
      component: Upload
    },
    {
      path: '/payment_misc',
      name: 'MiscPayments',
      component: MiscPayments
    },
    {
      path: '/agreement',
      name: 'Agreement',
      component: Agreement
    },
    {
      path: '/submission',
      name: 'Submission',
      component: Submission
    },
    {
      path: '/site_map',
      name: 'SiteMap',
      component: SiteMap
    },
    {
      path: '/search/:search',
      name: 'Search',
      component: Search
    },
    {
      path: '*',
      redirect: '/welcome'
    }
  ]
})
